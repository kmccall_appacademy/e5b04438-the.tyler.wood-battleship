class Board
  attr_accessor :grid

  def self.default_grid
    Array.new(10) { Array.new(10) }
  end

  def initialize(grid = Board.default_grid)
    @grid = grid
  end

  def display
    puts @grid
  end

  def in_range(pos)

  end

  def count
    @grid.inject(0) { |acc, el| acc + el.count(:s) }
  end

  def empty?(pos = nil)
    if pos
      return true if self[pos] == nil
      false
    else
      self.count == 0
    end
  end

  def full?
    @grid.count * @grid[0].count == self.count
  end

  def place_random_ship
    row = rand(@grid.count)
    col = rand(@grid[0].count)
    if !self.full?
      placed = false
      until placed
        if self[[row,col]] != :s
          self[[row,col]] = :s
          placed = true
        else
          row = rand(@grid.count)
          col = rand(@grid[0].count)
        end
      end
    elsif self.full?
      raise 'board is full'
    end

  end

  def [](pos)
    row, col = pos
    @grid[row][col]
  end

  def []=(pos, mark)
    row, col = pos
    @grid[row][col] = mark
  end

  def won?
    if self.count > 0
      false
    else
      true
    end
  end

end
